package com.company.stocks.repository;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.company.stocks.entity.Stocks;

@Repository
public interface StocksRepository extends JpaRepository<Stocks, Long> {
	
	@Query("select s from Stocks s where s.stockCode = ?1")
	Stocks findByStockCode(String stockCode);

}
