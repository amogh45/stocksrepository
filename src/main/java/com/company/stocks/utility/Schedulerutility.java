package com.company.stocks.utility;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.stereotype.Component;

import com.company.stocks.entity.Stocks;
import com.company.stocks.repository.StocksRepository;

@Component
public class Schedulerutility {
	
	@Autowired
	private StocksRepository repo;
	
	@Scheduled(fixedRate = 1000*60*5)
	public void batchProcess() {
		
		List<Stocks> stocksList = repo.findAll();
		
		List<Stocks> updateStockList = new ArrayList<Stocks>();
		for(Stocks stocks : stocksList) {
			stocks.setPrice(stocks.getPrice() + 1);
		}
		
		repo.saveAll(stocksList);
		
	}

}
