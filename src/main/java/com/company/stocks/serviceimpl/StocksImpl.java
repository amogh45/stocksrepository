package com.company.stocks.serviceimpl;

import java.util.Optional;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.company.stocks.dto.StockResponseDto;
import com.company.stocks.entity.Stocks;
import com.company.stocks.repository.StocksRepository;

@Service
public class StocksImpl {
	
	@Autowired
	private StocksRepository stocksRepository;

	public StockResponseDto getStockDetails(String stockCode) {
		StockResponseDto stockResponseDto = new StockResponseDto();
		Stocks stocks = stocksRepository.findByStockCode(stockCode);
		BeanUtils.copyProperties(stocks, stockResponseDto);
		
		return stockResponseDto;
	}
	
	

}
