package com.company.stocks.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import com.company.stocks.dto.StockResponseDto;
import com.company.stocks.serviceimpl.StocksImpl;

@RestController
public class StocksController {
	
	@Autowired
	private StocksImpl impl;
	
	@GetMapping("/stocks/{stockcode}")
	public ResponseEntity<StockResponseDto> getStockDetails(@PathVariable("stockcode") String stockCode) {
		
		
		StockResponseDto responseDto = impl.getStockDetails(stockCode);
		
		return new ResponseEntity<StockResponseDto>(responseDto, HttpStatus.OK);
		
	}

}
